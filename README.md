## **Projeto AWS**
----
## **Disciplina**
Plataformas de Desenvolvimento e Infraestrutura para Nuvem

### **Docente**
Benigno Batista Júnior

### **Discentes**:
- Ricardo Pereira da Silva
- Sérgio


## **Apresentação**:
----
O objetivo deste pequeno projeto é demonstrar a utilização dos recursos de Cloud AWS para permitir que uma aplicação legada (esta app) possua endpoints disponíveis para serem consumidos por serviços de integração diversos e possam adicionar usuários ao sistema sem fazer uso da interface Web legada.

Esta aplicação legada possui uma interface Web que permite as operações de CRUD de usuários.

Todas as operações de CRUD são acionadas à partir de métodos HTTP convencionais com a passagem de parâmetros na forma de submissão de formulários web.

Com o objetivo de criar um aplicativo mobile que possa interagir com as operações de CRUD, sem que o app legado seja modificado, um endpoint *REST* será criado usando o serviço de *API Gateway* da **AWS**.


## **Serviços AWS Utilizados**:


| Serviço     | Endereço                                                                       | Objetivo                                             |   |   |
|-------------|--------------------------------------------------------------------------------|------------------------------------------------------|---|---|
| Beanstalk   | http://projetoufscar2-env.eba-zn55h5gz.sa-east-1.elasticbeanstalk.com/         | Publicar o app legado                                |   |   |
| S3          | https://s3.console.aws.amazon.com/s3/home?region=sa-east-1                     | Utilizado como repositório de versionamento de apps  |   |   |
| Api Gateway | https://sa-east-1.console.aws.amazon.com/apigateway/main/apis?region=sa-east-1 | Expõe operações CRUD legadas em uma API REST moderna |   |   |

## **Detalhamento**:
----

### **Criação do app**
1. Utilizado tutorial Spring Boot Web app:
    https://www.baeldung.com/spring-boot-crud-thymeleaf
2. Uma versão deste tutorial foi alterada e armazenada no bitbucket
    https://bitbucket.org/ricardopsilva/projeto-aws/src/master/
3. Faça o clone do repositório para sua máquina e em um terminal digite
    
        ./mvnw clean package
        
4. Isto irá gerar alguns pacotes .jar no diretório target



### **Armazenamento dos fontes no S3**
1. Acessar portal S3:
    https://s3.console.aws.amazon.com/s3/home?region=sa-east-1
2. Crie um bucket chamado *projeto-ufscar* na região sa-east-1
3. Desmarque a opção *Block all public access*
4. Ative a opção *Bucket Versioning*
5. Crie um diretório chamado "java"
6. Faça upload dos arquivo *spring-boot-crud-exec.jar* gerados no diretório *target* do passo anterior.
7. Após o término do upload, volte à listagem do bucket e selecione o arquivo, em seguida clique no botão *Copy URL*
    




### **Criação do projeto no Beanstalk**
1. Acessar a console Elastic Beanstalk:
    https://console.aws.amazon.com/elasticbeanstalk
2. Clique no botão "Create Application"
3. Informe o nome da aplicação: *projeto-ufscar*
4. Escolha a plataforma *java*
5. Na seção *Application code* escolha a opção *Upload your code*
6. Na seção *Source code origin* marque a opção *Public S3 URL* e informe o valor do endereço do bucket S3 copiado no passo anterior
7. Clique sobre o botão "Create Application"
8. Aguarde a criação do environment e a publicação do projeto

> **ATENÇÃO:**    
Após a finalização do environment, a aplicação vai apresentar erro **500**.
Isso ocorre porque o Elastic beanstalk por padrão fornece uma instância EC2 que executa o Nginx como proxy reverso e escuta a porta 80. O Nginx por padrão encaminha a solicitação para o aplicativo em execução na porta interna 5000.
Portanto, precisamos realizar aluns ajustes para que nosso aplicativo Spring Boot seja executado na porta 5000 e não na padrão 8080.

#### **Configurando a variável de ambiente SERVER_PORT**
1. No dashboar do beanstalk do lado esquerdo, clique em *configuration*
2. Existem várias categorias exibidas aqui. As variáveis de ambiente pertencem à categoria *Software*. Então, clique em *Edit*.
3. Na seção de *Environment properties* adicione uma propriedade com o nome **SERVER_PORT** e valor **5000**.
4. Clique em Aplicare aguarde sua aplicação reiniciar.


### **Testanto o projeto**
1. Na seção de *environments* do Beanstalk, escolha o aplicativo *ProjetoUfscar* e clique no link logo abaixo de seu nome
    http://projetoufscar2-env.eba-zn55h5gz.sa-east-1.elasticbeanstalk.com/




### Criando WEB API para o projeto legado
1. faça login no console do API Gateway: 
    https://sa-east-1.console.aws.amazon.com/apigateway/main/apis?region=sa-east-1
2. Acione o botão "Create API"
3. Escolha a opção "REST API" e acione o botão "Build"
4. No campo "API Name" informe o valor **UfscarAddUser**
5. No campo "EndPoint Type" escolha o valor "Edge Optimized"
6. Clique em "Create API"
7. Após a API ser criada, acesse seus detalhes e, na opção suspensa "Actions" escolha **Create method**
8. Escolha o verbo "POST" e faça os seguintes ajustes:
    - Selecione verbo recém criado e escolha a opção **HTTP** no campo "Integration Type
    - Marque a opção **Use HTTP Proxy integration**
    - No campo "HTTP method" escolha o verbo "POST"
    - No campo "Endpoint URL" cole a URL da aplicação deployada no Beanstalk e adicione "/adduser":
        
        http://projetoufscar-env.eba-ifi5pxfx.sa-east-1.elasticbeanstalk.com/adduser

    - Em "Content Handling" escolha **Passthrough**
9. Agora volte para a seção "Method Execution" e selecione o link da caixa **Method Request**
10. Na seção "HTTP Request Headers" adicione o parâmetro com o nome "Content-Type"
11. volte para a seção "Method Execution" e selecione o link da caixa "Integration Request"
12. Abra a seção **HTTP Headers** e adicione os seguintes dados:
    - Name: **Content-Type**
    - Mapped from: **method.request.header.Content-Type**
    - Acione o ícone **Create** (check mark)
13. Agora volte para a seção "Method Execution" e acione o íncone de **Test** (raio azul)
14. Na seção de **Headers** insira o valor
    
    `Content-Type: application/x-www-form-urlencoded`

15. Na seção de **Body** insira o valor
    
    `name=jose&email=jose.silva@gmail.com`
16. Volte (ou abra uma nova) para a aba onde a aplicação do Beanstalk está sendo executada e acione o *refresh* da página
17. O usuário inserido pelo **API Gateway** deve estar listado na interface web da app.

